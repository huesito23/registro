package com.prueba.registro.service;

import com.prueba.registro.dto.PhoneDTO;
import com.prueba.registro.dto.UserCreationDTO;
import com.prueba.registro.dto.UserDTO;
import com.prueba.registro.dto.UserInformationDTO;
import com.prueba.registro.exception.PassWordException;
import com.prueba.registro.model.Person;
import com.prueba.registro.model.Phone;
import com.prueba.registro.repository.UserRepository;
import com.prueba.registro.service.impl.UserServiceImpl;
import com.prueba.registro.util.Constant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.util.ReflectionTestUtils.setField;


class UserServiceTest {

    private UserService userService;


    @Mock
    private UserRepository userRepository;



    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        userService =
                new UserServiceImpl(userRepository);

        setField(userService, "expresion", "^([A-Z]{1}[a-z]+)[0-9]{2}$", String.class);

    }




 @Test
    void userCreationOk(){

     UserDTO userRequest = new UserDTO();
        userRequest.setEmail("marco.pizarrof@gmail.com");
        userRequest.setName("marco");
        userRequest.setPassword("Marco30");

        List<PhoneDTO> phones = new ArrayList<>();
        PhoneDTO phoneRequest = new PhoneDTO();
        phoneRequest.setContryCode("1");
        phoneRequest.setCityCode("56");
        phoneRequest.setNumber("991704580");
        phones.add(phoneRequest);
        userRequest.setPhones(phones);


        Person person = new Person();
        LocalDateTime date = LocalDateTime.now();
         person.setLastLogin(date);

        person.setPassword(userRequest.getPassword());

        person.setActive(true);
        person.setEmail(userRequest.getEmail());
        person.setName(userRequest.getName());
        person.setModified(date);
        person.setCreated(date);


        List<Phone> phones2 = new ArrayList<>();

        userRequest.getPhones().forEach(val ->{
            Phone phone2 = new Phone();
            phone2.setCityCode(val.getCityCode());
            phone2.setContryCode(val.getContryCode());
            phone2.setNumber(val.getNumber());
            phone2.setPerson(person);
            phones2.add(phone2);
        });
        person.setPhones(phones2);


        Mockito.when(userRepository.save(any(Person.class)))
                .thenReturn(person);

     UserCreationDTO userResponse = userService.userCreation(userRequest);

        assertThat(userResponse).isNotNull();
    }

    @Test
    void userCreationNOk1(){
        PassWordException exception = assertThrows(PassWordException.class, () -> {

            UserDTO userRequest = new UserDTO();
            userRequest.setPassword("3A");

            userService.userCreation(userRequest);

        });

        assertTrue(exception.getMessage().contains(Constant.ERROR_MESSAGE_PASSWORD));

    }


    @Test
    void userInfoOK(){
        String token = "Bearer " +
                "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJQZXJlczEwIiwic3ViIjoicGVyZXNAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOlsiUk9MRV9VU0VSIl0sImlhdCI6MTY0MTM1NjE3OCwiZXhwIjoxNjQxMzU2Nzc4fQ.Z84IQ_oWsMvkDbruQwHhlgKvEEhmiqZZZuGkjqz30v8g718xH7fp0Qky0wg_epP_MNk0N9a_Md-kPEp5GnVDMg";

        List<Person> personList= new ArrayList<>();
        Person person = new Person();
        LocalDateTime date = LocalDateTime.now();
        person.setLastLogin(date);

        person.setPassword("1234");

        person.setActive(true);
        person.setEmail("mar@gmail.com");
        person.setName("marco");
        person.setModified(date);
        person.setCreated(date);

        List<Phone> phones2 = new ArrayList<>();


            Phone phone2 = new Phone();
            phone2.setCityCode("23");
            phone2.setContryCode("23");
            phone2.setNumber("34");
            phone2.setPerson(person);
            phones2.add(phone2);

        person.setPhones(phones2);

        personList.add(person);


        Mockito.when(userRepository.countByEmailAndActive(any(String.class),any(Boolean.class)))
                .thenReturn(1L);


        Mockito.when(userRepository.findAllByActive(any(Boolean.class)))
                .thenReturn(personList);


        List<UserInformationDTO> list = userService.userInfo(token);


        assertThat(list).isNotNull();
    }





}
