package com.prueba.registro.service;

import com.prueba.registro.dto.AccountDTO;
import com.prueba.registro.dto.TokenDTO;
import com.prueba.registro.repository.UserRepository;
import com.prueba.registro.service.impl.LoginServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;


class LoginServiceTest {

    private LoginService loginService;


    @Mock
    private UserRepository userRepository;



    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        loginService =
                new LoginServiceImpl(userRepository);


    }




 @Test
    void tokenOK(){

     AccountDTO accountDTO = new AccountDTO();


     accountDTO.setPassword("12345");
     accountDTO.setEmail("mar@gmail.com");

        Mockito.when(userRepository.countByEmailAndPasswordAndActive(anyString(),anyString(), anyBoolean()))
                .thenReturn(1L);

     TokenDTO tokenDTO = loginService.token(accountDTO);

        assertThat(tokenDTO).isNotNull();
    }


    @Test
    void tokenNOK(){

        AccountDTO accountDTO = new AccountDTO();


        accountDTO.setPassword("12345");
        accountDTO.setEmail("mar@gmail.com");

        Mockito.when(userRepository.countByEmailAndPasswordAndActive(anyString(),anyString(), anyBoolean()))
                .thenReturn(0L);

        TokenDTO tokenDTO = loginService.token(accountDTO);

        assertThat(tokenDTO).isNotNull();
    }






}
