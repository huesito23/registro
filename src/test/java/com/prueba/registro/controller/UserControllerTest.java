package com.prueba.registro.controller;

import com.prueba.registro.dto.PhoneDTO;
import com.prueba.registro.dto.UserCreationDTO;
import com.prueba.registro.dto.UserDTO;
import com.prueba.registro.dto.UserInformationDTO;
import com.prueba.registro.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

    }




    @Test
    void userCreation(){

        UserDTO userRequest = new UserDTO();
        userRequest.setEmail("marco@gmail.com");
        userRequest.setName("marco");
        userRequest.setPassword("ggggg");

        List<PhoneDTO> phones = new ArrayList<>();
        PhoneDTO phoneRequest = new PhoneDTO();
        phoneRequest.setContryCode("1");
        phoneRequest.setCityCode("56");
        phoneRequest.setNumber("991704580");
        phones.add(phoneRequest);
        userRequest.setPhones(phones);

        UserCreationDTO userResponse = new UserCreationDTO();
        userResponse.setCreated(LocalDateTime.now());
        userResponse.setLastLogin(LocalDateTime.now());
        userResponse.setActive(true);
        userResponse.setLastLogin(LocalDateTime.now());
        userResponse.setId(1L);
        userResponse.setMessage("OK");

        when(userService.userCreation(userRequest)).thenReturn(userResponse);

        userResponse = userController.userCreation(userRequest);

        assertThat(userResponse).isNotNull();
    }


    @Test
    void userInfo(){
        List<UserInformationDTO> list = new ArrayList<>();
        UserInformationDTO userInformationDTO = new UserInformationDTO();
        LocalDateTime date = LocalDateTime.now();
        userInformationDTO.setModified(date);
        userInformationDTO.setName("Marco");
        userInformationDTO.setCreated(date);
        userInformationDTO.setId(1L);
        userInformationDTO.setEmail("mar@gmail.com");
        userInformationDTO.setActive(true);
        userInformationDTO.setLastLogin(date);

        List<PhoneDTO> phones = new ArrayList<>();
        PhoneDTO phoneRequest = new PhoneDTO();
        phoneRequest.setContryCode("1");
        phoneRequest.setCityCode("56");
        phoneRequest.setNumber("991704580");
        phones.add(phoneRequest);

        userInformationDTO.setPhones(phones);

        list.add(userInformationDTO);
        when(userService.userInfo("zzzzzzzzz")).thenReturn(list);

        list = userController.userInfo("zzzzzzzzz");

        assertThat(list).isNotNull();
    }



}
