package com.prueba.registro.controller;

import com.prueba.registro.dto.AccountDTO;
import com.prueba.registro.dto.TokenDTO;
import com.prueba.registro.service.LoginService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class LoginControllerTest {

    @Mock
    private LoginService loginService;

    @InjectMocks
    private LoginController loginController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

    }




    @Test
    void login(){
        TokenDTO tokenDTO = new TokenDTO();
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setEmail("marco@gmail.com");
        accountDTO.setPassword("12345");
        tokenDTO.setToken("FFFFF");
        tokenDTO.setMessage("OK");

        when(loginService.token(accountDTO)).thenReturn(tokenDTO);

        tokenDTO = loginController.login(accountDTO);

        assertThat(tokenDTO).isNotNull();
    }





}
