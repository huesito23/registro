package com.prueba.registro.controller;

import com.prueba.registro.dto.UserDTO;
import com.prueba.registro.dto.UserCreationDTO;
import com.prueba.registro.dto.UserInformationDTO;
import com.prueba.registro.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Validated
public class UserController {

    private final UserService userService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserCreationDTO userCreation(
            @Valid @RequestBody UserDTO userDTO){
        return userService.userCreation(userDTO);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UserInformationDTO> userInfo(
            @RequestHeader("Authorization") String token){
        return userService.userInfo(token);
    }
}
