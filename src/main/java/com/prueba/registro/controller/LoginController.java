package com.prueba.registro.controller;

import com.prueba.registro.dto.AccountDTO;
import com.prueba.registro.dto.TokenDTO;
import com.prueba.registro.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/login")
@RequiredArgsConstructor
@Validated
public class LoginController {

    private final LoginService loginService;

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public TokenDTO login(
            @Valid @RequestBody AccountDTO accountDTO){
        return loginService.token(accountDTO);
    }
}
