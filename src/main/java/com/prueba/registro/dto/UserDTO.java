package com.prueba.registro.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    @NotNull(message = "El campo nombre es requerido")
    private String name;

    @Email(message = "Correo invalido")
    @NotNull(message = "El campo email es requerido")
    private String email;

    @NotNull(message = "El campo clave es requerida")
    private  String password;

    private List<PhoneDTO> phones;
}
