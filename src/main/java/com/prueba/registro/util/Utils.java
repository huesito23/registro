package com.prueba.registro.util;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

public class Utils {

    private Utils(){}


    public static Boolean passValidate(String pass , String expresion){
        return pass.matches(expresion);
    }

    public static String getEmailfromJWT(String jwt) throws JSONException {
        String[] splitString = jwt.split("\\.");
        String base64EncodedBody = splitString[1];
        Base64 base64Url = new Base64(true);
        String body = new String(base64Url.decode(base64EncodedBody));
        JSONObject bodyJson = new JSONObject(body);

        return bodyJson.getString("sub");
    }


}
