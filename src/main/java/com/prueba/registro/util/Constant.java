package com.prueba.registro.util;

public class Constant {
    private  Constant(){}

    public static   final String HEADER = "Authorization";
    public static final String PREFIX = "Bearer ";
    public static final String SECRET = "mySecretKey";
    public static final Integer CODE_NO_DATA_FOUND = 404;
    public static final Integer CODE_OK = 200;
    public static final Integer CODE_BAD_REQUEST= 400;
    public static final String ERROR_TITLE_EMAIL = "correo";
    public static final String ERROR_MESSAGE_EMAIL = "El correo ya se encuentra registrado";
    public static final String ERROR_EMAIL = "El Correo es inválido";
    public static final String ERROR_VALIDATION_DATA = "Ha ocurrido un error al validar los datos de entrada";

    public static final String ERROR_TITLE_PASSWORD = "Password";
    public static final String ERROR_MESSAGE_PASSWORD = "El formato de la clave es incorrecto deber tener lo siguiente: (Una Mayuscula, letras minúsculas, y dos numeros) ";


    public static final String ERROR_TITLE_TOKEN = "Token";
    public static final String ERROR_MESSAGE_TOKEN= "Token inválido ";
    public static final String MESSAGE= "Usuario registrado correctamente ";
    public static final String ERROR_MESSAGE_USER= "Usuario No Registrado";
}
