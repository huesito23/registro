package com.prueba.registro.repository;


import com.prueba.registro.model.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<Person, Long> {

    List<Person> findAllByActive(Boolean active);
    Long countByEmailAndPasswordAndActive(String email, String password,Boolean active);
    Long countByEmailAndActive(String email,Boolean active);
}
