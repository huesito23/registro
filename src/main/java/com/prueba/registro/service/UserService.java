package com.prueba.registro.service;

import com.prueba.registro.dto.UserDTO;
import com.prueba.registro.dto.UserCreationDTO;
import com.prueba.registro.dto.UserInformationDTO;

import java.util.List;

public interface UserService {
    UserCreationDTO userCreation(UserDTO userDTO);
    List<UserInformationDTO> userInfo(String token);

}
