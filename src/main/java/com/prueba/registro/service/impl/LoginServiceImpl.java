package com.prueba.registro.service.impl;

import com.prueba.registro.dto.AccountDTO;
import com.prueba.registro.dto.TokenDTO;
import com.prueba.registro.repository.UserRepository;
import com.prueba.registro.service.LoginService;
import com.prueba.registro.util.Constant;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final UserRepository userRepository;


    /**
     *
     * @param accountDTO
     * @return TokenDTO
     * @author Marco Pizarro <marco.pizarrof@gmail.com>
     * @date  24-03-2021
     * @description Servicio que arma el token con el usuario y clave obtenido
     */
    @Override
    public TokenDTO token(AccountDTO accountDTO) {

        Long count=
                userRepository.countByEmailAndPasswordAndActive(accountDTO.getEmail(),
                        accountDTO.getPassword(),true);

        return Optional.ofNullable(count)
                .filter(val-> count > 0)
                .map(value -> new TokenDTO(getToken(accountDTO),null))
                .orElse(new TokenDTO(null, Constant.ERROR_MESSAGE_USER));


    }

    private String getToken(AccountDTO accountDTO) {

        String secretKey = "mySecretKey";
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                .commaSeparatedStringToAuthorityList("ROLE_USER");

        String token = Jwts
                .builder()
                .setId("recordJWT")
                .setSubject(accountDTO.getEmail())
                .claim("authorities",
                        grantedAuthorities.stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000))
                .signWith(SignatureAlgorithm.HS512,
                        secretKey.getBytes()).compact();

        return "Bearer " + token;
    }
}
