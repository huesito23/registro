package com.prueba.registro.service.impl;


import com.prueba.registro.dto.AccountDTO;
import com.prueba.registro.dto.PhoneDTO;
import com.prueba.registro.dto.UserInformationDTO;
import com.prueba.registro.exception.PassWordException;
import com.prueba.registro.exception.TokenException;
import com.prueba.registro.model.Person;
import com.prueba.registro.model.Phone;
import com.prueba.registro.repository.UserRepository;
import com.prueba.registro.dto.UserDTO;
import com.prueba.registro.dto.UserCreationDTO;
import com.prueba.registro.service.UserService;
import com.prueba.registro.util.Constant;
import com.prueba.registro.util.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Value("${expresion.regular.pass}")
    private String expresion;


    /**
     *
     * @param userDTO
     * @return UserCreationDTO
     * @author Marco Pizarro <marco.pizarrof@gmail.com>
     * @date 24-03-2021
     * @description Servicio que registra usuarios
     */
    @Override
    public UserCreationDTO userCreation(UserDTO userDTO) {
       Boolean flag = Utils.passValidate(userDTO.getPassword(),expresion);
        return Optional.of(flag)
               .filter(validate -> flag)
               .map(r -> saveUserCreation(userDTO))
                .orElseThrow(() -> new PassWordException(Constant.ERROR_MESSAGE_PASSWORD));
    }




    /**
     *
     * @param userDTO
     * @return UserCreationDTO
     * @author Marco Pizarro <marco.pizarrof@gmail.com>
     * @date 24-03-2021
     * @description Metodo que realiza el guardado del registro obtenido
     */
    private UserCreationDTO saveUserCreation(UserDTO userDTO){
        Person person  = new Person();

        LocalDateTime date = LocalDateTime.now();
        person.setPassword(userDTO.getPassword());

        person.setActive(true);
        person.setEmail(userDTO.getEmail());
        person.setName(userDTO.getName());
        person.setModified(date);
        person.setLastLogin(date);
        person.setCreated(date);

        Optional.ofNullable(userDTO.getPhones())
                .filter(tii->!userDTO.getPhones().isEmpty())
                .ifPresent(ti->{
                    List<Phone> phones = new ArrayList<>();
                    userDTO.getPhones().forEach(val ->{
                        Phone phone = new Phone();
                        phone.setCityCode(val.getCityCode());
                        phone.setContryCode(val.getContryCode());
                        phone.setNumber(val.getNumber());
                        phone.setPerson(person);
                        phones.add(phone);
                    });
                    person.setPhones(phones);
                });

        Person per = userRepository.save(person);

        return new UserCreationDTO(per.getId(),Constant.MESSAGE,per.getCreated(),
                per.getModified(),per.getLastLogin(),per.getActive());


    }


    /**
     *
     * @param token
     * @return  List<UserInformationDTO>
     * @author Marco Pizarro <marco.pizarrof@gmail.com>
     * @date 24-03-2021
     * @description Servicio que obtiene el listado de usuarios registrados
     */
    @Override
    public List<UserInformationDTO> userInfo(String token) {

        String email =  Utils.getEmailfromJWT(token);
        Long count = userRepository.countByEmailAndActive(email, true);

       return Optional.ofNullable(count)
                .filter(resp -> count > 0)
                .map(xi->{
                    List<Person> list = userRepository.findAllByActive(true);
                    return Optional.ofNullable(list)
                            .filter(val-> !list.isEmpty())
                            .map(value ->setUserInfo(list))
                            .orElse(new ArrayList<>());
                }) .orElseThrow(() -> new TokenException(Constant.ERROR_MESSAGE_TOKEN));

    }


    private  List<UserInformationDTO> setUserInfo( List<Person> list){

        List<UserInformationDTO>  userInformationDTOS = new ArrayList<>();
        list.forEach(x ->{

            UserInformationDTO userInformationDTO = new UserInformationDTO();
            userInformationDTO.setCreated(x.getCreated());
            userInformationDTO.setActive(x.getActive());
            userInformationDTO.setId(x.getId());
            userInformationDTO.setName(x.getName());
            userInformationDTO.setLastLogin(x.getLastLogin());
            userInformationDTO.setModified(x.getModified());
            List<PhoneDTO> phones = new ArrayList<>();

            Optional.ofNullable(x.getPhones())
                    .filter(validate-> !x.getPhones().isEmpty())
                    .ifPresent(xii-> xii.forEach(z ->{
                        PhoneDTO phoneDTO = new PhoneDTO();
                        phoneDTO.setCityCode(z.getCityCode());
                        phoneDTO.setNumber(z.getNumber());
                        phoneDTO.setContryCode(z.getContryCode());
                        phones.add(phoneDTO);
                    }));

            userInformationDTO.setPhones(phones);
            userInformationDTOS.add(userInformationDTO);
        });
        return userInformationDTOS;
    }
}
