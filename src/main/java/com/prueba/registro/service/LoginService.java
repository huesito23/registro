package com.prueba.registro.service;

import com.prueba.registro.dto.AccountDTO;
import com.prueba.registro.dto.TokenDTO;

public interface LoginService {

    TokenDTO token(AccountDTO accountDTO);
}
