package com.prueba.registro.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionGlobalResponse {

    private final String timestamp = LocalDateTime.now().toString();
    private int status;
    private String error;
    private List<String> details;
}
