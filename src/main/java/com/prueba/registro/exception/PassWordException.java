package com.prueba.registro.exception;

public class PassWordException extends RuntimeException {
    public PassWordException(String message) {
        super(message);
    }
}
