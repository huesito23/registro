package com.prueba.registro.exception.handler;

import com.prueba.registro.exception.EmailException;
import com.prueba.registro.exception.ExceptionConstraintResponse;
import com.prueba.registro.exception.ExceptionGlobalResponse;
import com.prueba.registro.exception.ExceptionResponse;
import com.prueba.registro.exception.GeneralException;
import com.prueba.registro.exception.PassWordException;
import com.prueba.registro.exception.TokenException;
import com.prueba.registro.util.Constant;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@ControllerAdvice
@RestController
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(EmailException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ExceptionResponse handleExceptionNoDataFoundEmail() {
        return new ExceptionResponse(
                Constant.CODE_OK,
                Constant.ERROR_TITLE_EMAIL,
                Constant.ERROR_MESSAGE_EMAIL);}


    @ExceptionHandler(value = {ConstraintViolationException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionConstraintResponse handleValidationsErrorsException(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
        List<String> details = new ArrayList<>();
        violations.forEach(x -> details.add(x.getMessage()));

        return new ExceptionConstraintResponse(HttpStatus.BAD_REQUEST.value(),
                Constant.ERROR_VALIDATION_DATA,
                details);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionResponse handleMethodArgumentNotValidException (MethodArgumentNotValidException e) {
        return new ExceptionResponse(HttpStatus.BAD_REQUEST.value(),
                Constant.ERROR_VALIDATION_DATA,e.getBindingResult().getAllErrors().get(0).getDefaultMessage());

    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public ExceptionResponse handleDataIntegrityViolationException() {
        return new ExceptionResponse(HttpStatus.OK.value(),
                Constant.ERROR_TITLE_EMAIL, Constant.ERROR_MESSAGE_EMAIL);
    }


    @ExceptionHandler(value = {HttpMessageNotReadableException.class })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ExceptionGlobalResponse handleValidationsNotReadableException(HttpMessageNotReadableException e) {
        return new ExceptionGlobalResponse(HttpStatus.BAD_REQUEST.value(), Constant.ERROR_VALIDATION_DATA,
                Collections.singletonList(e.getLocalizedMessage()));
    }

    @ExceptionHandler(PassWordException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ExceptionResponse handleExceptionPassWord() {
        return new ExceptionResponse(
                Constant.CODE_BAD_REQUEST,
                Constant.ERROR_TITLE_PASSWORD,
                Constant.ERROR_MESSAGE_PASSWORD);}


    @ExceptionHandler(GeneralException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ExceptionResponse generalException(Integer code, String title ,String message) {
        return new ExceptionResponse(code, title, message);}



    @ExceptionHandler(TokenException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ExceptionResponse handleExceptionToken() {
        return new ExceptionResponse(
                Constant.CODE_BAD_REQUEST,
                Constant.ERROR_TITLE_TOKEN,
                Constant.ERROR_MESSAGE_TOKEN);}

}
