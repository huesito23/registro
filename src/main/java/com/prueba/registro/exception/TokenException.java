package com.prueba.registro.exception;

public class TokenException extends RuntimeException {
    public TokenException(String message) {
        super(message);
    }
}
