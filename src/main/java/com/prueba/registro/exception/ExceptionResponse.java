package com.prueba.registro.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionResponse {
    private final String timestamp = LocalDateTime.now().toString();
    private Integer idResponse;
    private String titleError;
    private String messageError;
}
