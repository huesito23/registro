package com.prueba.registro.exception;

public class GeneralException extends RuntimeException {
    public GeneralException(String message) {
        super(message);
    }
}
