# Microservicio - Registro de Usuario


ms para el registro y obtención de usuarios


1. [Ownership](/readme.md#1-ownership)
2. [Operaciones](/readme.md#2-operaciones)
3. [Especificación Técnica](/readme.md#3-especificaci%C3%B3n-t%C3%A9cnica)
4. [Explicación](/readme.md#4-ejecuci%C3%B3n)
5. [Eventos](/readme.md#5-eventos)
6. [Ciclo de Vida](/readme.md#6-ciclo-de-vida)


## 1. Ownership

### 1.1. Technical Owner
  - Responsable: Marco Pizarro 
  - Contacto: marco.pizarrof@gmail.com

## 2. Operaciones

| Operación   | Descripción Capacidad                                            |
|-------------|------------------------------------------------------------------| 
| POST /login | Operacion encargada de obtener el token del usuario registrado   |
| POST /user  | Operacion encargada de registrar un nuevo usuario                |
| GET /user   | Operacion encargada de obtener el listado de usuario registrados |


## 3. Especificación Técnica
http://localhost:8080/swagger-ui.html
  
## 4. Explicación

### 4.1. Orden Ejecución

| Items | Descripción del orden de ejecución                                               | Operación    |
|-------|----------------------------------------------------------------------------------|--------------|
| 1     | Se registra el usuario                                                           | (POST)/user  |
| 2     | El usuario registrado ingresa (obtiene token)                                    | (POST)/login |
| 3     | Se obtiene el listado de usuario registrado (debe venir con el jwt en el header) | (GET)/user   |


### 4.2. Repositorio de Datos

| Repositorio | Descripción |
|-------------| ------  |
| H2          | Base de Datos en memoria que permite realizar caching para el Microservicio  |

### 4.3. Backends


## 5. Eventos
En esta sección se especifican los eventos que utiliza el microservicio como parte su lógica de negocio.
| Evento | Descripción | publicador/suscriptor |
| ------  | ------  | ------  |
| journal  | Evento para journalizar  | publicador  |

## 6. Ciclo de Vida
En esta sección se especifica el procedimiento para disponibilizar el microservicio.

### 6.1. Compilación
Detalla la forma en que el microservicio debe ser compilado:

./gradlew build

### 6.2. Configuraciones
Detalla las configuraciones necesarias para que el microservicio pueda operar:

#### 6.2.1. Propiedades

#### 6.2.2. Secretos

### 6.3. Ejecución

#### 6.3.1. Contenedor
Especifica la forma en que se ejecuta la aplicación en un contenedor docker:

docker run --publish 8080:8080 registro:latest


